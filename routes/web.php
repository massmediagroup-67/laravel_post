<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome')->name('home');
});

Route::get('/show', 'PostController@show');
Route::post('/search', 'PostController@search');
Route::get('/create', 'PostController@create');
Route::post('/create', 'PostController@store');
