class TagsSelect {

    constructor(tagSelectSelector, tagListSelector, tagInputSelector)
    {
        this.tagSelectSelector = tagSelectSelector;
        this.tagListSelector = tagListSelector;
        this.tagInputSelector = tagInputSelector;
    }

    setAddButton(button)
    {
        let that = this;
        $(button).click(function () {
            that.addTag();
        })
    }

    addTag()
    {
        let val = $(this.tagInputSelector).val();
        this.tagId += 1;
        $(this.tagListSelector).append('<li>' +
            '<div class="tag">' +
            val +
            '</div>' +
            '</li>');
        $(this.tagSelectSelector).append(
            '<option selected>' + val + '</option>'
        );
    }
}

export default TagsSelect

