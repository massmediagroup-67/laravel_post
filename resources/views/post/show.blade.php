@extends('base')

@section('content')
    <div class="container p-2">
        <form method="POST" action="{{ action('PostController@search') }}" class="w-100">
            {{ csrf_field() }}
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Search by tag or post title" name="needle">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>

        <div class="container-fluid">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h1>Post List</h1>
                    @foreach ($posts as $post)
                        <div class="mt-3 post">
                            <h3>{{ $post->title }}</h3>
                            <div class="description">
                                {{ $post->description }}
                            </div>
                            <h5>Tags:</h5>
                            <div class="tags">
                                <ul id="tag-list">
                                    @foreach ($post->tags as $tag)
                                        <li>
                                            <div class="tag">
                                                {{ $tag->name }}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-2"></div>
            </div>
        </div>
    </div>
@endsection