@extends('base')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form method="POST" class="w-100">
                    {{ csrf_field() }}
                    @component('post.form-group', ['label' => 'Title:', 'name' => 'title'])
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title">
                    @endcomponent
                    @component('post.form-group', ['label' => 'Description:', 'name' => 'description'])
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" rows="3" name="description"></textarea>
                    @endcomponent
                    @component('post.form-group', ['label' => 'Add new tag:', 'name' => 'tags.*'])
                        <div class="input-group mb-3">
                            <input id="tag-input" type="text" class="form-control @error('tags.*') is-invalid @enderror">
                            <div class="input-group-append">
                                <button type="button"  class="btn btn-primary" id="tag-add-button"><i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    @endcomponent
                    <div class="form-group">
                        <label for="description">Tags:</label>
                        <ul id="tag-list">
                        </ul>
                        {{-- select el for save tags and sending to controller--}}
                        <select  id="tag-select" multiple name="tags[]" class="hidden-select">
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection