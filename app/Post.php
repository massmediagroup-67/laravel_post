<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';

    public function tags()
    {
        return $this->hasMany('App\Tag');
    }

    /**
     * Return Posts where $needle in post title or post's tag name
     * @param $needle
     * @return mixed
     */
    public static function search($needle)
    {
        $inTags = Tag::select('post_id')->where('name', 'like', "%$needle%");
        $inTitle = Post::select('id')->where('title', 'like', "%$needle%");
        return Post::whereIn('id', $inTags)->orWhereIn('id', $inTitle)->distinct()->get();
    }
}


