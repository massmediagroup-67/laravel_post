<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePost;
use App\Post;
use App\Services\PostCreator;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function show()
    {
        return view('post.show', ['posts' => Post::all()]);
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(StorePost $request, PostCreator $pc)
    {
        if ($request->validated()) {
            $pc->create($request->input());
        }
        return view('post.create');
    }

    public function search(Request $request)
    {
        $needle = $request->input()['needle'];
        $posts = Post::search($needle);

        return view('post.show', ['posts' => $posts]);
    }
}
