<?php

namespace App\Http\Middleware;

use Closure;

class BlockIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($request->ip(), config('app.blockedIp'))) {
            return response()->make(view('deniedAccess'));
        }

        return $next($request);
    }
}
