<?php


namespace App\Services;

use App\Post;

class PostCreator
{
    public function create(array $data)
    {
        $post = new Post();
        $post->title = $data['title'];
        $post->description = $data['description'];
        $post->save();

        $tags = array_map(function ($el) {
            return ['name' => $el];
        }, $data['tags']);

        $post->tags()->createMany($tags);
    }
}
