<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tag';
    protected $fillable = ['name', 'posts_id'];

    public function posts()
    {
        return $this->belongsTo('App\Post');
    }
}
